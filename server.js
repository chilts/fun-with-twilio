// ----------------------------------------------------------------------------

// core
const http = require('http')
const path = require('path')

// npm
const express = require('express')
const bodyParser = require('body-parser')
const twilio = require('twilio')

// ----------------------------------------------------------------------------

// setup
const isProd = ( process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'staging' )
const isDev = !isProd
const port = process.env.PORT || '3000'

// twilio
const accountSid = process.env.TWILIO_ACCOUNT_SID
const authToken = process.env.TWILIO_AUTH_TOKEN
const client = twilio(accountSid, authToken)

const { VoiceResponse } = twilio.twiml

// we use let since we want to be able to call `/reset`
let call = {}
let won = {}
let lost = {}
let like = {}
let unknown = {}

// ----------------------------------------------------------------------------
// helpers

function sendXml(req, res, twiml) {
  res.writeHead(200, { 'Content-Type': 'text/xml' })
  console.log('Response: ' + twiml.toString())
  res.end(twiml.toString() + '\n')
}

// ----------------------------------------------------------------------------
// app

const app = express()
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')
app.enable('trust proxy')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

app.get('/', (req, res) => {
  res.render('index', {
    call,
    won,
    lost,
    like,
    unknown,
  })
})

app.get('/won.json', (req, res) => {
  res.json(won)
})

app.get('/lost.json', (req, res) => {
  res.json(lost)
})

app.get('/like.json', (req, res) => {
  res.json(like)
})

app.get('/unknown.json', (req, res) => {
  res.json(unknown)
})

app.get('/reset', (req, res) => {
  call = {}
  won = {}
  lost = {}
  like = {}
  unknown = {}
  res.send('OK')
})

app.use('/api', (req, res, next) => {
  // check the 'AccountSid' is correct, otherwise we'll just 404
  if ( isProd ) {
    if ( req.body.AccountSid !== accountSid ) {
      res.status(403).send('403 - Forbidden')
      return
    }
  }
  next()
})

app.post('/api/initiate.xml', (req, res) => {
  console.log('/api/initiate.xml -> Initiate', JSON.stringify(req.body))
  const body = req.body

  // remember this call
  call[body.CallSid] = {
    status: 'initiating',
    authenticated: false,
    twilio: body,
  }

  const twiml = new VoiceResponse()

  // Use the <Gather> verb to collect user input
  const gather = twiml.gather({
    numDigits: 1,
    input: 'dtmf',
    action: '/api/process-option.xml',
  })
  gather.say('Welcome to Fun with Twilio. Please press 1 2 or 3.')

  // If the user doesn't enter anything, we'll just hang up
  twiml.say("Thank you. Goodbye.")
  twiml.hangup()

  sendXml(req, res, twiml)
})

app.post('/api/process-option.xml', (req, res) => {
  console.log(' -> ProcessOption', JSON.stringify(req.body))
  const body = req.body
  const digits = body.Digits

  const twiml = new VoiceResponse()

  // First Competition - If the user enters an even number, they win, else they lose.
  if ( digits === '1' ) {
    // Use the <Gather> verb to collect two more digits
    const gather = twiml.gather({
      digits: 2,
      input: 'dtmf',
      action: '/api/number-competition.xml',
    })
    gather.say('Please enter a two digit number followed by the pound sign.')

    // If the user doesn't enter anything, we'll just hang up
    twiml.say("Thank you. Goodbye.")
    twiml.hangup()
  }
  else if ( digits === '2' ) {
    // Get the person's name and what they like.
    const gather = twiml.gather({
      input: 'speech',
      speechTimeout: 'auto',
      action: '/api/what-you-like.xml',
    })
    gather.say('Please say your name and something you like.')

    // If the user doesn't enter anything, we'll just hang up
    twiml.say("Thank you. Goodbye.")
    twiml.hangup()
  }
  else if ( digits === '3' ) {
    // Send them either Rick Astley or say 'Thank you'

    // only connect to me with a small probability
    if ( Math.random() < 0.1 ) {
      twiml.dial('021891681');
      twiml.say('Connecting you now, please hold the line');
    }
    else {
      // RickRoll them all!
      twiml.play("http://demo.twilio.com/docs/classic.mp3")
    }
  }
  else {
    twiml.say("Unknown option - thank you. Goodbye.")
    twiml.hangup()
    unknown[body.CallSid] = {
      CallSid: body.CallSid,
      From: body.From,
      Caller: body.Caller,
    }
  }

  sendXml(req, res, twiml)
})

app.post('/api/number-competition.xml', (req, res) => {
  console.log(' -> NumberCompetition', JSON.stringify(req.body))
  const body = req.body
  const digits = body.Digits

  const twiml = new VoiceResponse()

  // firstly, make the two digits into a number
  const guess = digits|0

  if ( ( guess % 2 ) === 0 ) {
    twiml.say("Winner winner chicken dinner!")
    twiml.hangup()
    won[body.CallSid] = {
      CallSid: body.CallSid,
      From: body.From,
      Caller: body.Caller,
    }
  }
  else {
    twiml.say("Bad luck this time, you got the turkey.")
    twiml.hangup()
    lost[body.CallSid] = {
      CallSid: body.CallSid,
      From: body.From,
      Caller: body.Caller,
    }
  }

  sendXml(req, res, twiml)
})

app.post('/api/what-you-like.xml', (req, res) => {
  console.log(' -> WhatYouLike', JSON.stringify(req.body))
  const body = req.body

  const twiml = new VoiceResponse()

  // get the speech and confidence
  const SpeechResult = body.SpeechResult
  const Confidence = body.Confidence

  // save it into like
  like[body.CallSid] = {
    CallSid: body.CallSid,
    From: body.From,
    Caller: body.Caller,
    SpeechResult: body.SpeechResult,
    Confidence: body.Confidence,
  }

  twiml.say("Thank you for your response.")
  twiml.hangup()

  sendXml(req, res, twiml)
})

// ----------------------------------------------------------------------------

// This is a WebHook, rather than a Call event.
app.post('/api/status.xml', (req, res) => {
  const body = req.body
  console.log(' -> Status Change', JSON.stringify(body))

  // if the call has finished
  if ( body.CallStatus === 'completed' ) {
    delete call[body.CallSid]
  }
  else {
    console.log(''.padStart(79, '-'))
    console.log('CallStatus=' + body.CallStatus)
    console.log(''.padStart(79, '-'))
  }

  res.send('ok')
})

// This is a WebHook, rather than a Call event.
app.post('/api/error.xml', (req, res) => {
  const body = req.body
  console.log(' -> Error', JSON.stringify(body))
  res.send('ok')
})

// ----------------------------------------------------------------------------
// server

const server = http.createServer()
server.on('request', app)
server.listen(port, () => {
  console.log('Listening on port %s', port)
})

// ----------------------------------------------------------------------------
