#!/bin/bash
## ----------------------------------------------------------------------------

set -e

BASE_URL='http://localhost:3000'

curl --silent $BASE_URL/reset > /dev/null

## ----------------------------------------------------------------------------

echo "01 - Won currently empty"
curl --silent $BASE_URL/won.json | json_pp --json_opt=canonical,pretty - > test/01-won-got.json
diff test/empty.json test/01-won-got.json

echo "01 - Lost currently empty"
curl --silent $BASE_URL/lost.json | json_pp --json_opt=canonical,pretty - > test/01-lost-got.json
diff test/empty.json test/01-lost-got.json

# test the initial.xml
# "Called":"+6468819032","ToState":"Woodville","CallerCountry":"NZ","Direction":"inbound","Timestamp":"Mon, 25 Feb 2019 09:13:03 +0000","CallbackSource":"call-progress-events","CallerState":"","ToZip":"","SequenceNumber":"0","To":"+6468819032","CallSid":"CA77e865401f5ae174223c16eb6c29dbc4","ToCountry":"NZ","CallerZip":"","CalledZip":"","ApiVersion":"2010-04-01","CallStatus":"completed","CalledCity":"","Duration":"1","From":"+6421891681","CallDuration":"15","AccountSid":"AC0d72bff4771de3a8165226a838e3fc90","CalledCountry":"NZ","CallerCity":"","ToCity":"","FromCountry":"NZ","Caller":"+6421891681","FromCity":"","CalledState":"Woodville","FromZip":"","FromState":""
echo "01 - Initiate"
curl -X POST --data "CallSid=01&CallStatus=ringing&Caller=0271234567" --silent http://localhost:3000/api/initiate.xml | xmllint --format - > test/01-initiate-got.xml
diff test/01-initiate-exp.xml test/01-initiate-got.xml

echo "02a - Process Option - 8 (Unknown Option)"
curl -X POST --data "CallSid=01&CallStatus=in-progress&Caller=0271234567&Digits=8" --silent http://localhost:3000/api/process-option.xml | xmllint --format - > test/02a-process-option-got.xml
diff test/02a-process-option-exp.xml test/02a-process-option-got.xml

echo "02 - Process Option"
curl -X POST --data "CallSid=01&CallStatus=in-progress&Caller=0271234567&Digits=1" --silent http://localhost:3000/api/process-option.xml | xmllint --format - > test/02-process-option-got.xml
diff test/02-process-option-exp.xml test/02-process-option-got.xml

echo "03 - Number Competition - Winner!"
curl -X POST --data "CallSid=01&CallStatus=in-progress&Caller=0271234567&Digits=98" --silent http://localhost:3000/api/number-competition.xml | xmllint --format - > test/03a-number-competition-got.xml
diff test/03a-number-competition-exp.xml test/03a-number-competition-got.xml

echo "03 - Number Competition - Loser"
curl -X POST --data "CallSid=27&CallStatus=in-progress&Caller=0271234567&Digits=27" --silent http://localhost:3000/api/number-competition.xml | xmllint --format - > test/03b-number-competition-got.xml
diff test/03b-number-competition-exp.xml test/03b-number-competition-got.xml

echo "01 - One winner"
curl --silent $BASE_URL/won.json | json_pp --json_opt=canonical,pretty - > test/01-won-got.json
diff test/01-won-exp.json test/01-won-got.json

echo "01 - One loser"
curl --silent $BASE_URL/lost.json | json_pp --json_opt=canonical,pretty - > test/01-lost-got.json
diff test/01-lost-exp.json test/01-lost-got.json

## ----------------------------------------------------------------------------

echo "09 - Likes currently empty"
curl --silent $BASE_URL/like.json | json_pp --json_opt=canonical,pretty - > test/09-like-got.json
diff test/09-like-exp.json test/09-like-got.json

echo "10 - Process Option - 2"
curl -X POST --data "CallSid=01&CallStatus=in-progress&Caller=0271234567&Digits=2" --silent http://localhost:3000/api/process-option.xml | xmllint --format - > test/10-process-option-got.xml
diff test/10-process-option-exp.xml test/10-process-option-got.xml

echo "11 - What you like"
curl -X POST --data "CallSid=01&CallStatus=in-progress&Caller=0271234567&Confidence=0.8&SpeechResult=Hello" --silent http://localhost:3000/api/what-you-like.xml | xmllint --format - > test/11-what-you-like-got.xml
diff test/11-what-you-like-exp.xml test/11-what-you-like-got.xml

echo "12 - Likes with one entry"
curl --silent $BASE_URL/like.json | json_pp --json_opt=canonical,pretty - > test/12-like-got.json
diff test/12-like-exp.json test/12-like-got.json

## ----------------------------------------------------------------------------

echo "10 - Process Option - 3"
curl -X POST --data "CallSid=01&CallStatus=in-progress&Caller=0271234567&Digits=3" --silent http://localhost:3000/api/process-option.xml | xmllint --format -

## ----------------------------------------------------------------------------

curl --silent $BASE_URL/reset > /dev/null

## ----------------------------------------------------------------------------
